﻿using System.Drawing;
using System.Windows.Forms;

namespace JetBrains_Icons
{
    public partial class FileTypeListItem : UserControl
    {
        public FileTypeListItem()
        {
            InitializeComponent();
        }

        public void SetName(string name)
        {
            this.nameLabel.Text = name;
        }

        public void SetDesc(string description)
        {
            this.descriptionLabel.Text = description;
        }

        public void SetImage(string productName)
        {
            this.pictureBox1.Image =
                (Bitmap) Properties.Resources.ResourceManager.GetObject(productName, Properties.Resources.Culture);
        }
    }

}