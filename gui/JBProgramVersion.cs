﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace JetBrains_Icons
{
    internal class JbProgramVersion
    {
        public string Version;
        public string OpenPath;

        public JbProgramVersion(RegistryKey key, ExtProgram program)
        {
            this.OpenPath = key.OpenSubKey("shell\\open\\command")?.GetValue("") as string;
            this.Version = key.Name.Substring(program.Name.Length);
        }
    }
}
