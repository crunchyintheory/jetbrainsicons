﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JetBrains_Icons
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
            List<Control> controls = new();
            for (int i = 0; i < FileType.AllTypes.Count; i++)
            {
                FileType type = FileType.AllTypes[i];
                FileTypeListItem item = new();
                item.SetName($".{type.Ext}");
                item.SetDesc(type.Description);
                item.SetImage(type.Programs[0].Name);
                int offset = item.Height * i;
                Point itemLocation = item.Location;
                itemLocation.Y = offset;
                item.Location = itemLocation;
                controls.Add(item);
            }

            this.panel1.Controls.AddRange(controls.ToArray());
            foreach (Control control in controls)
            {
                control.BringToFront();
                control.Show();
            }
        }

    }
}
