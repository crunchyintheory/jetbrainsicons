﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace JetBrains_Icons
{
    internal class ExtProgram
    {
        public static readonly List<ExtProgram> AllPrograms = new List<ExtProgram>();

        public string Name;
        public string LongName;
        private List<JbProgramVersion> _versions;
        public ExtProgram(string name, string longName)
        {
            this.Name = name;
            this.LongName = longName;
        }

        public List<JbProgramVersion> GetRegistryVersions()
        {
            if (this._versions != null)
                return this._versions;

            this._versions = new List<JbProgramVersion>();

            IEnumerable<string> subKeyNames = Program.HklmClasses.GetSubKeyNames().Where(x => x.StartsWith(this.LongName));
            foreach (string name in subKeyNames)
            {
                this._versions.Add(new JbProgramVersion(Program.HklmClasses.OpenSubKey(name), this));
            }
            return this._versions;
        }

        public string NameRegistryKey(JbProgramVersion version, string fileExt)
        {
            return $"{this.Name}{version.Version}.{fileExt}";
        }
    }
}
