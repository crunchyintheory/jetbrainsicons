﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using Microsoft.Win32;

namespace JetBrains_Icons
{
    internal class FileType
    {
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        internal record FileTypeDB
        {
            public record Program
            {
                public string name { get; set; }
                public string longName { get; set; }
                public static implicit operator ExtProgram(Program p)
                {
                    string longName = p.longName ?? p.name;
                    return new ExtProgram(p.name, longName);
                }
            }

            public record FileType
            {
                public string ext { get; set; }
                public string icon { get; set; }
                public string[] openable { get; set; }
                public string description { get; set; }
                public static implicit operator JetBrains_Icons.FileType(FileType ft)
                {
                    string icon = ft.icon ?? ft.ext;
                    string description = ft.description ?? $"{ft.ext.ToUpper()} File";
                    return new JetBrains_Icons.FileType(ft.ext, icon, ft.openable, description);
                }
            }

            public Program[] programs { get; set; }
            public FileType[] types { get; set; }
        }

        private static readonly List<FileType> _allTypes = new List<FileType>();

        public string Ext;
        public string Icon;
        public string Description;
        public List<ExtProgram> Programs;

        public static List<FileType> AllTypes
        {
            get
            {
                if(_allTypes.Count == 0)
                    GetFileTypes();
                return _allTypes;
            }
        }

        private static void GetFileTypes()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            const string resourceName = "JetBrains_Icons.types.json";

            using Stream stream = assembly.GetManifestResourceStream(resourceName);
            using StreamReader reader = new StreamReader(stream);
            string jsonTypes = reader.ReadToEnd();
            FileTypeDB data = JsonSerializer.Deserialize<FileTypeDB>(jsonTypes) ?? throw new ArgumentNullException("Could not parse types.json data");
            foreach (ExtProgram program in data.programs)
            {
                ExtProgram.AllPrograms.Add(program);
            }
            foreach (FileType type in data.types)
            {
                _allTypes.Add(type);
            }
        }

        public static void GetRegistryTypes()
        {
            //Computer\HKEY_LOCAL_MACHINE\SOFTWARE\Classes\
        }

        public FileType(string name, string icon, string[] openable, string description)
        {
            this.Ext = name;
            this.Icon = icon;
            this.Description = description;
            this.Programs = ExtProgram.AllPrograms.Where(x => openable.Contains(x.Name)).ToList();
        }

    }
}
