package main

import (
	"encoding/xml"
	"os"
	"path/filepath"
)

type SVGRoot struct {
	XMLName         xml.Name         `xml:"svg"`
	LinearGradients []LinearGradient `xml:"linearGradient"`
	Paths           []Path           `xml:"path,omitempty"`
}

type LinearGradient struct {
	XMLName       xml.Name `xml:"linearGradient"`
	ID            string   `xml:"id,attr,omitempty"`
	GradientUnits string   `xml:"gradientUnits,attr,omitempty"`
	X1            string   `xml:"x1,attr,omitempty"`
	X2            string   `xml:"x2,attr,omitempty"`
	Y1            string   `xml:"y1,attr,omitempty"`
	Y2            string   `xml:"y2,attr,omitempty"`
	Stops         []Stop   `xml:"stop,omitempty"`
	XLinkHref     string   `xml:"http://www.w3.org/1999/xlink href,attr,omitempty"`
}

type Stop struct {
	XMLName   xml.Name `xml:"stop"`
	Offset    string   `xml:"offset,attr,omitempty"`
	StopColor string   `xml:"stop-color,attr,omitempty"`
}

type Path struct {
	XMLName xml.Name `xml:"path"`
	D       string   `xml:"d,attr,omitempty"`
	ID      string   `xml:"id,attr,omitempty"`
	Fill    string   `xml:"fill,attr,omitempty"`
}

func (program *FileTypeDBProgram) GetGradientsAndShapes() (string, string) {
	if program.Gradients != "" {
		return program.Gradients, program.Shapes
	}

	svgFile, _ := os.ReadFile(filepath.Join(cwd, "./sourceicons/"+program.Name+"_icon.svg"))

	var svg SVGRoot

	_ = xml.Unmarshal(svgFile, &svg)

	paths, _ := xml.Marshal(svg.Paths[:len(svg.Paths)-1])
	grads, _ := xml.Marshal(svg.LinearGradients)

	return string(grads), string(paths)
}
