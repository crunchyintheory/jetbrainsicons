package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"text/template"
)

var icon *template.Template
var iconSmall *template.Template
var projIcon *template.Template
var slnIcon *template.Template

func createSVGs() {
	icon, _ = template.ParseFiles(filepath.Join(cwd, "./templates/icon.svg.gtpl"))
	iconSmall, _ = template.ParseFiles(filepath.Join(cwd, "./templates/icon_small.svg.gtpl"))
	projIcon, _ = template.ParseFiles(filepath.Join(cwd, "./templates/proj.svg.gtpl"))
	slnIcon, _ = template.ParseFiles(filepath.Join(cwd, "./templates/sln.svg.gtpl"))

	content, _ := ioutil.ReadFile("../gui/types.json")
	var payload FileTypeDB
	_ = json.Unmarshal(content, &payload)

	for _, typ := range payload.Types {
		if typ.Icon == "" {
			typ.Icon = typ.Ext
		}
		typ.Icon = strings.ToUpper(typ.Icon)

		icon := icon
		if len(typ.Icon) > 4 && typ.Icon[len(typ.Icon)-4:] == "PROJ" {
			icon = projIcon
			typ.Icon = typ.Icon[:len(typ.Icon)-4]
		} else if typ.Icon == "SLN" {
			icon = slnIcon
		}

		data := FileTypeIconData{
			Type: typ,
		}

		for _, prog := range typ.Openable {
			fmt.Println("Creating SVG for " + typ.Ext + " opened in " + prog)

			file, _ := os.OpenFile(svgLgDir+"/"+prog+"."+typ.Ext+".svg", os.O_TRUNC|os.O_CREATE, 0600)

			data.Program = payload.findProgram(prog)
			data.Text = data.renderText("lg")
			data.TextScale = data.getScaleForText("lg")
			data.Gradients, data.Shapes = data.Program.GetGradientsAndShapes()

			_ = icon.Execute(file, data)
			_ = file.Close()

			if data.Program.NonCommercial || data.Program.Education {
				data.Shapes = ""
				file, _ = os.OpenFile(svgLgDir+"/"+prog+".nc."+typ.Ext+".svg", os.O_TRUNC|os.O_CREATE, 0600)
				_ = icon.Execute(file, data)
				_ = file.Close()
			}

			file, _ = os.OpenFile(svgSmDir+"/"+prog+"."+typ.Ext+".svg", os.O_TRUNC|os.O_CREATE|os.O_WRONLY, 0600)

			data.Text = data.renderText("sm")
			data.TextScale = data.getScaleForText("sm")

			_ = iconSmall.Execute(file, data)
			_ = file.Close()
		}
	}
}

func (data *FileTypeIconData) getScaleForText(size string) float32 {
	switch size {
	case "sm":
		if !data.HasCustomIcon && len(data.Type.Icon) == 3 {
			return 0.7
		} else {
			return 1
		}
	case "lg":
		if !data.HasCustomIcon && len(data.Type.Icon) == 3 {
			return 0.1
		} else {
			return 0.1328
		}
	default:
		return 0
	}
}

func (data *FileTypeIconData) findTextTemplate(size string) (*template.Template, error) {
	data.HasCustomIcon = true
	text, err := template.ParseFiles(filepath.Join(cwd, "./templates/partials/text/"+data.Type.Icon+"_"+size+".svg.gtpl"))

	if err != nil {
		text, err = template.ParseFiles(filepath.Join(cwd, "./templates/partials/text/"+data.Type.Icon+".svg.gtpl"))
		if err != nil {
			data.HasCustomIcon = false
			textSize := "sm"
			if len(data.Type.Icon) >= 4 {
				textSize = "lg"
				data.Text1 = data.Type.Icon[:2]
				data.Text2 = data.Type.Icon[2:4]
			}
			text, err = template.ParseFiles(filepath.Join(cwd, "./templates/partials/text/"+size+"_"+textSize+".svg.gtpl"))
			if err != nil {
				text, err = template.ParseFiles(filepath.Join(cwd, "./templates/partials/text/default_"+textSize+".svg.gtpl"))
				if err != nil {
					text, err = template.ParseFiles(filepath.Join(cwd, "./templates/partials/text/default.svg.gtpl"))
				}
			}
		}
	}

	return text, err
}

func (data *FileTypeIconData) renderText(size string) string {
	text, _ := data.findTextTemplate(size)

	textBuf := new(bytes.Buffer)

	_ = text.Execute(textBuf, data)

	return textBuf.String()
}

func (data *FileTypeDB) findProgram(name string) FileTypeDBProgram {
	for _, program := range data.Programs {
		if program.Name == name {
			return program
		}
	}
	panic("Could not find program " + name)
}
