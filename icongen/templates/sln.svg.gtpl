<svg height="105" viewBox="0 0 105 105" width="105" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g transform="scale(0.85) translate(12 11.5)">
        {{ .Gradients }}
        <path d="m7 14.5h91v76h-91z" fill="url(#a)"/>
        <g transform="scale(1.25 1) translate(-12 0)">
            {{ .Shapes }}
        </g>
        <path d="m10 17.5h85v70h-85z" opacity="0.9"/>
        <g fill="#fff">
            <g transform="translate(17.5 67.5) scale(0.1328)">
                {{ .Text }}
            </g>
            <path d="m20 73.75h22.5v3.75h-22.5z"/>
        </g>
    </g>
</svg>