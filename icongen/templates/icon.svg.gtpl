<svg height="105" viewBox="0 0 105 105" width="105" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g transform="scale(0.85) translate(12 11.5)">
        {{ .Gradients }}
        <path d="m19 7h49v3l15 15h3v73h-67z" fill="url(#a)" display="none"/>
        <path d="m19 7h50.5l16.5 16.5v74.5h-67z" fill="url(#a)"/>
        <g transform="scale(1 1.25) translate(0 -12)">
            {{ .Shapes }}
        </g>
        <path d="m22 10h46l15 15v70h-61z" opacity="0.9"/>
        <g fill="#fff">
            <g transform="translate(25.5 75) scale({{ .TextScale }})">
                {{ .Text }}
            </g>
            <path d="m28.02 85h22.5v3.75h-22.5z"/>
        </g>
    </g>
</svg>