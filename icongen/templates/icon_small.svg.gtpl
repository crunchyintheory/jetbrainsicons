<svg width="512" height="512" xmlns="http://www.w3.org/2000/svg">
    <rect width="512" height="512"
          style="fill:#000000;stroke:{{.Program.Color}};stroke-width:64">
    </rect>
    <svg height="30" width="200">
        <g transform="translate(72, 288) scale({{ .TextScale }})">
            {{ .Text }}
        </g>
    </svg>
    <path d="m96 384h192v32h-192z" fill="#ffffff"/>
</svg>