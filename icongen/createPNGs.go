package main

import (
	"fmt"
	"github.com/srwiley/oksvg"
	"github.com/srwiley/rasterx"
	"image"
	"image/png"
	"os"
	"path/filepath"
	"strconv"
)

func createPNGs() {
	//smSizes := []int{16, 20, 24}
	lgSizes := []int{32, 40, 48, 64, 256}
	svgs, _ := os.ReadDir(svgLgDir)
	for _, dirEntry := range svgs {
		if dirEntry.IsDir() || dirEntry.Name()[0] == '.' {
			continue
		}

		rstream, _ := os.Open(filepath.Join(cwd, svgLgDir, dirEntry.Name()))

		icon, _ := oksvg.ReadIconStream(rstream)

		for _, size := range lgSizes {
			makeIconForSize(icon, dirEntry.Name(), size)
		}
	}
}

func makeIconForSize(icon *oksvg.SvgIcon, name string, size int) {
	fmt.Println("Converting " + name + " to PNG of size " + strconv.Itoa(size))
	icon.SetTarget(0, 0, float64(size), float64(size))
	rgba := image.NewRGBA(image.Rect(0, 0, size, size))
	icon.Draw(rasterx.NewDasher(size, size, rasterx.NewScannerGV(size, size, rgba, rgba.Bounds())), 1)

	out, _ := os.Create(filepath.Join(cwd, pngDir, name[:len(name)-3]+strconv.Itoa(size)+".png"))
	defer out.Close()

	_ = png.Encode(out, rgba)
}
