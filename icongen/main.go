package main

import "os"

type FileTypeIconData struct {
	Program       FileTypeDBProgram
	Type          FileTypeDBFileType
	Text          string
	TextScale     float32
	Gradients     string
	Shapes        string
	Text1         string
	Text2         string
	HasCustomIcon bool
}

type FileTypeDB struct {
	Programs []FileTypeDBProgram  `json:"programs"`
	Types    []FileTypeDBFileType `json:"types"`
}

type FileTypeDBProgram struct {
	Name          string `json:"name"`
	LongName      string `json:"longName,omitempty"`
	Color         string `json:"color"`
	NonCommercial bool   `json:"nonCommercial"`
	Education     bool   `json:"edu"`
	Gradients     string
	Shapes        string
}

type FileTypeDBFileType struct {
	Ext         string   `json:"ext"`
	Icon        string   `json:"icon,omitempty"`
	Openable    []string `json:"openable"`
	Description string   `json:"description,omitempty"`
}

var cwd = ""
var svgLgDir = "out/svg/lg"
var svgSmDir = "out/svg/sm"
var pngDir = "out/png"
var icoDir = "out/ico"

func main() {
	cwd, _ = os.Getwd()

	createSVGs()

	createPNGs()

	//createICOs()
}
